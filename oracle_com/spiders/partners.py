# -*- coding: utf-8 -*-
import re
import json
import scrapy


class PartnersSpider(scrapy.Spider):
    name = 'partners'
    allowed_domains = ['oracle.com']
    start_urls = ['https://partner-finder.oracle.com/catalog/wcm/templates-r/fragments-sc/main/search_results.jspx?json_params_filter=%7B%22filtersArr%22%3A%5B%22xscCompanyLocation%3AAll%22%5D%7D&SearchId=search2&isCallback=true']


    def parse(self, response):
        try:
            json_text = response.css('#jsonForMap ::attr(value)').extract_first()
            data = json.loads(json_text)
        except Exception as e:
            self.logger.warning('Unable to parse json', exc_info=True)

        partners = ['https://partner-finder.oracle.com/catalog/scr/Partner/{}.html'.format(p['id']) for p in data['profiles']]

        for p in partners:
            yield scrapy.Request(p, callback=self.parse_partner)


    def parse_partner(self, response):
        d = {}

        d['url'] = response.url
        d['name'] = _strip(response.css('h1 ::text').extract_first())
        d['phone'] = _strip(response.css('.o-partner-businesscard--links .o-partner-businesscard--reveal .o-partner-phonenum ::text').extract_first())
        d['website'] = _strip(response.css('.o-partner-businesscard--address a ::attr(href)').extract_first())
        d['address'] = _clear_and_join(response.xpath('//*[@class="o-partner-businesscard--address"]//*[not(self::a)]/text()').extract())

        if d['website'] == 'http://' or d['website'] == 'https://':
            d['website'] = None

        if d['phone'] == 'No contact number available.':
            d['phone'] = None

        return d


def _strip(string, default=None):
    if string:
        return string.strip()
    return default


def _clear_and_join(lst, sep='\n'):
    lst = [re.sub('[\xa0\r\t ]+', ' ', t, flags=re.DOTALL).strip() for t in lst]
    lst = list(filter(None, [re.sub('\s*\n\s*', sep, t, flags=re.DOTALL).strip() for t in lst]))

    text = sep.join(lst)

    return text
